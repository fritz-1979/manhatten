package main

import (
	"configuration"
	"router"
)

func main() {
	r := router.GetRouter()
	r.Run(":" + configuration.GetPort())
}
