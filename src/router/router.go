package router

import (
	"github.com/gin-gonic/gin"

	"rest"
)

func GetRouter() *gin.Engine {
	router := gin.Default()

	router.GET("/", rest.SayHelloHandler)
	router.GET("/users/all", rest.GetAllHandler)
	router.GET("/users", rest.GetGenericAttributeHandler)
	router.POST("/users", rest.PostHandler)
	router.DELETE("/users", rest.DeleteGenericAttributeHandler)
	router.DELETE("/users/all", rest.DeleteAllHandler)
	return router
}
