package rest

import (
	"fmt"
	"net/http"

	"infrastructure"
	. "utils"

	"github.com/gin-gonic/gin"
)

func SayHelloHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "Hello! I am the go app running for you :-)"})
}

func GetAllHandler(c *gin.Context) {
	var results interface{}
	results = infrastructure.GetAll()
	c.JSON(http.StatusOK, results)
}

func GetGenericAttributeHandler(c *gin.Context) {
	method := "GetGenericAttributeHandler"
	attribute := c.Query("attribute")
	value := c.Query("value")
	LogIt(INFO, method, fmt.Sprintf("attribute=%s, value=%s", attribute, value))
	results, err := infrastructure.FetchByGenericAttr(attribute, value)
	if err != nil {
		message := fmt.Sprintf("Entity with attribute '%s' and value '%s' not found", attribute, value)
		LogIt(ERROR, method, message)
		c.JSON(http.StatusNotFound, gin.H{"message": message})
	} else {
		c.JSON(http.StatusOK, results)
	}
}
