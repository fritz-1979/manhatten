package rest

import (
	"fmt"
	"net/http"

	"infrastructure"
	. "utils"

	"github.com/gin-gonic/gin"
)

func DeleteGenericAttributeHandler(c *gin.Context) {
	method := "DeleteGenericAttributeHandler"
	attribute := c.Query("attribute")
	value := c.Query("value")
	parameter := fmt.Sprintf("attribute=%s, value=%s", attribute, value)
	LogIt(INFO, method, fmt.Sprintf("DeleteGenericAttributeHandler %s", parameter))
	err := infrastructure.DeleteByGenericAttr(attribute, value)
	if err != nil {
		message := fmt.Sprintf("Cannot delete entity with %s", parameter)
		LogIt(ERROR, method, message)
		c.JSON(http.StatusNotFound, gin.H{"message": message})
	} else {
		message := fmt.Sprintf("Successfully deleted entity with %s", parameter)
		LogIt(INFO, method, message)
		c.JSON(http.StatusOK, gin.H{"message": message})
	}
}

func DeleteAllHandler(c *gin.Context) {
	method := "DeleteAllHandler"
	info, err := infrastructure.DeleteAll()
	if err != nil {
		message := fmt.Sprintf("Cannot delete all entries")
		LogIt(ERROR, method, message)
		c.JSON(http.StatusNotFound, gin.H{"message": message})
	} else {
		message := fmt.Sprintf("Successfully deleted all entities")
		LogIt(INFO, method, message)
		c.JSON(http.StatusOK, gin.H{"message": info})
	}
}
