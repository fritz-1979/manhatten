package rest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"

	"infrastructure"
	. "utils"
)

func PostHandler(c *gin.Context) {
	method := "PostHandler"
	data, _ := ioutil.ReadAll(c.Request.Body)
	message := fmt.Sprintf("Receiving data: \n%s\n", string(data))
	LogIt(INFO, method, message)
	var doc interface{}
	err := json.Unmarshal(data, &doc)
	if err != nil {
		message := fmt.Sprintf("Error while unmarshalling json: '%+v'", err)
		LogIt(ERROR, method, message)
		c.JSON(http.StatusBadRequest, gin.H{"message": message})
	} else {
		infrastructure.Insert(doc)
		c.JSON(http.StatusOK, doc)
	}
}
